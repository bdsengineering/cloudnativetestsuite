@Library('jenkins-shared-libraries')
import com.blackducksoftware.jenkins.pipeline.Libraries

def libraries = new Libraries()
def properties
def clone = libraries.getSourceClone()
def utilities = libraries.getUtilities()

def targetNode = "docker"
def HIPCHAT_ROOM = "CloudNativeEngineering"
currentBuild.result = "SUCCESS"

node(targetNode){
  try{
    stage('Preparation'){
      if (env.WORKSPACE){
        dir(env.WORKSPACE){
          utilities.openPermsWorkspace()
          deleteDir()
        }
      }

      clone.cloneGitSourceCode(env,
                               'git@github.com:blackducksoftware/opssight-connector.git',
                               "${env.WORKSPACE}/opssight-connector",
                               "${env.SERV_BUILDER_GIT_CREDENTIALS}",
                               env.BRANCH).call()
    }

    stage('opsight-smoke-test.py'){
      try{
        sh "python opssight-connector/cloud-native-test-suite/pyfire/run.py"
      } catch (Exception e){
        currentBuild.result = "FAILURE"
        throw e
      }
    }
  } catch(Exception e){
    libraries.getEmail().emailOnFailure(env)
    throw e
  } finally {
    libraries.getHipChat().sendNotification(env, currentBuild, HIPCHAT_ROOM, '')

    if (env.WORKSPACE){
      dir(env.WORKSPACE){
        utilities.openPermsWorkspace()
        deleteDir()
      }
    }
  }
}
